Source: python-openapi-core
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Julian Gilbey <jdg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               help2man,
               pybuild-plugin-pyproject,
               python3-aiohttp,
               python3-aioitertools,
               python3-all,
               python3-django,
               python3-djangorestframework <!nocheck>,
               python3-falcon,
               python3-fastapi,
               python3-flask,
               python3-httpx <!nocheck>,
               python3-isodate,
               python3-jsonschema,
               python3-jsonschema-path,
               python3-more-itertools,
               python3-multidict,
               python3-openapi-schema-validator,
               python3-openapi-spec-validator,
               python3-parse,
               python3-poetry-core,
               python3-pytest <!nocheck>,
               python3-pytest-aiohttp <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-requests,
               python3-responses <!nocheck>,
               python3-sphinx,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-starlette,
               python3-strict-rfc3339 <!nocheck>,
               python3-werkzeug
Testsuite: autopkgtest-pkg-pybuild
Standards-Version: 4.7.0
Homepage: https://github.com/python-openapi/openapi-core
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-openapi-core
Vcs-Git: https://salsa.debian.org/python-team/packages/python-openapi-core.git
Rules-Requires-Root: no
Description: Client- and server-side support for OpenAPI specification
 This Python library adds client-side and server-side support for the
 OpenAPI v3.0 and v3.1 specification. It offers:
 .
   * validation and unmarshalling of request and response data (including
     webhooks)
   * integration with popular libraries (Requests and Werkzeug) and
     frameworks (Django, Falcon, Flask, Starlette)
   * customization with media type deserializers and format unmarshallers
   * security data providers (API keys, Cookie, Basic and Bearer HTTP
     authentications)

Package: python3-openapi-core
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-openapi-core-doc,
          python3-aiohttp,
          python3-aioitertools,
          python3-django,
          python3-falcon,
          python3-fastapi,
          python3-flask,
          python3-multidict,
          python3-requests,
          python3-starlette
Multi-Arch: foreign
Description: ${source:Synopsis} (Python 3)
 ${source:Extended-Description}
 .
 This package installs the library for Python 3.

Package: python-openapi-core-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: python3-openapi-core
Built-Using: ${sphinxdoc:Built-Using}
Multi-Arch: foreign
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This package contains HTML documentation.
Build-Profiles: <!nodoc>
